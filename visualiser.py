import argparse
import glob
import os
import shutil
import cv2
import numpy as np


def getArguments() -> argparse.Namespace:
    """
    Function to get command line arguments
    Returns:
        arguments (argparse.Namespace) : img_path | mask_path
    """
    args = argparse.ArgumentParser()
    args.add_argument("--img_path", required=True,
                      help="Path to the image folder")
    args.add_argument("--mask_path", required=True,
                      help="Path to the mask folder")
    arguments = args.parse_args()

    return arguments


def visualise(original_img: np.ndarray, mask_img: np.ndarray, filename: str) -> None:
    concat_images = np.concatenate((original_img, mask_img), axis=1)
    concat_resize = cv2.resize(concat_images, (1280, 720))
    cv2.imshow(f"Comparison {filename}", concat_resize)
    key = cv2.waitKey(0) & 0XFF
    if key == ord('q'):
        os._exit(0)
    elif key == ord('s'):
        cv2.imwrite(f"filtered_images/img/{filename}", original_img)
        cv2.imwrite(f"filtered_images/mask/{filename}", mask_img)
    else:
        pass
    cv2.destroyAllWindows()
    return


def main() -> None:
    cmdLine_arguments: argparse.Namespace = getArguments()
    img_path = cmdLine_arguments.img_path
    mask_path = cmdLine_arguments.mask_path

    if os.path.isdir("filtered_images"):
        shutil.rmtree("filtered_images")
    os.makedirs("filtered_images/img")
    os.makedirs("filtered_images/mask")
    for file in glob.glob(img_path + '/*'):
        filename = file.split('/')[-1]
        try:
            mask_file = f"{mask_path}/{filename}"
            mask_data = cv2.imread(mask_file)
            img_data = cv2.imread(file)
            visualise(img_data, mask_data, filename)
        except ValueError:
            print(
                f"Please check if the file exists in the directory: {mask_file}")
            os._exit(0)


if __name__ == '__main__':
    main()
    os._exit(0)
