## YOLACT Mask-segmentation

This is the yolact implementation of mask segmentation for Shutoko Highway Project 2021.

## Anaconda Environment Setup

Please install prerequisites libraries and dependencies using anaconda.
```
conda create env -f yolact_2021.yaml
```

Activate the Environment.
```
conda activate yolact_2021
```

## DCN build

Please go into DCNv2_latest folder of the repository and build the library.

```
cd DCNv2_latest/
python3 setup.py develop build
```

## Conversion of mask dataset into json format

```
shutoko_dataset = dataset_base.copy({
    'name': 'Shutoko_2021',
    'train_images': "./datasets/train2017/",
    'train_info': "./datasets/annotations/instances_train2017.json",
    'valid_images': "./datasets/val2017/",
    'valid_info': './datasets/annotations/instances_val2017.json',
    'class_names': ('anchor', 'a1', 'a2', 'nut_side', 'nut_top', 'bolt_side', 'bolt_top', 'sA1', 's2A1', 's3A1', 'sA2'),
    'label_map': { 1:  1,  2:  2,  3:  3, 4: 4, 5: 5, 6: 6, 7: 7, 8: 8, 9: 9, 10: 10, 11:11 },
    'has_gt': True,
})
```

## Training
```
CUDA_VISIBLE_DEVICES=0 python3 train.py --config=yolact_plus_resnet50_config --size 700 --epoch 100 --batch_size 2
```

## Inference

Please place the trained weights inside weights folder

```
python3 inference.py --source <img_file_dir> --weights <weight_dir> --visualise
```
