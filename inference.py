import numpy as np
from typing import Tuple
import glob
import matplotlib.pyplot as plt
import cv2
import torch.backends.cudnn as cudnn
import torch
import warnings
from collections import defaultdict
from utils.augmentations import FastBaseTransform
from layers.output_utils import postprocess
from yolact import Yolact
import data
import os
import argparse
warnings.filterwarnings("ignore")

color_cache = defaultdict(lambda: {})


def getArguments() -> argparse.Namespace:
    args = argparse.ArgumentParser()
    args.add_argument("--weights", required=True,
                      help="Path to the weights file")
    args.add_argument("--source", required=True,
                      help="Path to the data to inference")
    args.add_argument("--size", default=550, help="Input image size to model")
    arguments = args.parse_args()

    return arguments


def loadModel(weights_dir: str) -> Yolact:
    net_model = Yolact()
    net_model.load_weights(weights_dir)
    net_model.eval()
    net_model.cuda()
    # faster nms
    net_model.detect.use_fast_nms = True
    # class nms set to false by default
    net_model.detect.use_cross_class_nms = False
    return net_model


def get_color(j, on_gpu=None):
    global color_cache
    color_idx = (j * 1) % len(data.COLORS)
    if on_gpu is not None and color_idx in color_cache[on_gpu]:
        return color_cache[on_gpu][color_idx]
    else:
        color = data.COLORS[color_idx]
        # The image might come in as RGB or BRG, depending
        color = (color[2], color[1], color[0])
        if on_gpu is not None:
            color = torch.Tensor(color).to(on_gpu).float() / 255.
            color_cache[on_gpu][color_idx] = color
        return color


def inference(net_model: Yolact, image_file: str) -> Tuple[np.ndarray, np.ndarray]:
    mask_alpha = 1.0
    image_data = cv2.imread(image_file)
    frame = torch.from_numpy(image_data).cuda().float()
    frame_gpu = frame / 255.0
    batch = FastBaseTransform()(frame.unsqueeze(0))
    preds = net_model(batch)
    h, w, c = image_data.shape
    results = postprocess(preds, w, h, visualize_lincomb=False,
                          crop_masks=True, score_threshold=0.01)
    idx = results[1].argsort(0, descending=True)[:100]
    masks = results[3][idx]
    classes, scores, boxes = [x[idx].cpu().numpy() for x in results[:3]]
    num_dets_to_consider = min(100, classes.shape[0])
    for j in range(num_dets_to_consider):
        if scores[j] < 0.2:
            num_dets_to_consider = j
            break

    if num_dets_to_consider > 0:
        masks_det = masks[:num_dets_to_consider, :, :, None]
        colors = torch.cat([get_color(j, on_gpu=frame_gpu.device.index).view(
            1, 1, 1, 3) for j in classes[:num_dets_to_consider]], dim=0)
        masks_color = masks_det.repeat(1, 1, 1, 3) * colors * mask_alpha
        # This is 1 everywhere except for 1-mask_alpha where the mask is
        inv_alph_masks = masks_det * (-mask_alpha) + 1

        # I did the math for this on pen and paper. This whole block should be equivalent to:
        #    for j in range(num_dets_to_consider):
        #        img_gpu = img_gpu * inv_alph_masks[j] + masks_color[j]
        masks_color_summand = masks_color[0]

        if num_dets_to_consider > 1:
            inv_alph_cumul = inv_alph_masks[:(
                num_dets_to_consider - 1)].cumprod(dim=0)
            masks_color_cumul = masks_color[1:] * inv_alph_cumul
            masks_color_summand += masks_color_cumul.sum(dim=0)

        img_gpu = frame_gpu * inv_alph_masks.prod(dim=0) + masks_color_summand
        mask_image = masks_color_summand
        mask_gpu = (mask_image * 255).byte().cpu().numpy()

    else:
        mask_image = np.zeros((h, w, 3), np.uint8)
        mask_image[:] = (0, 0, 0)
        img_gpu = frame_gpu
        mask_gpu = mask_image

    img_gpu = (img_gpu * 255).byte().cpu().numpy()

    return img_gpu, mask_gpu


def main() -> None:
    cmd_args = getArguments()
    config_file = "yolact_plus_resnet50_config"
    data.set_cfg(config_file, int(cmd_args.size), rescore_bbox=True)
    print(f"Config file loaded: {data.cfg.name}")
    print(f"Input image size: {data.cfg.max_size}")

    with torch.no_grad():
        cudnn.fastest = True
        torch.set_default_tensor_type('torch.cuda.FloatTensor')
        yolact_model = loadModel(cmd_args.weights)
        for file in glob.glob(cmd_args.source + '*'):
            filename = file.split("/")[-1]
            f, axarr = plt.subplots(1, 2)
            img, mask = inference(yolact_model, file)
            axarr[0].imshow(img)
            axarr[1].imshow(mask)
            cv2_img = cv2.cvtColor(mask, cv2.COLOR_BGR2RGB)
            cv2.imshow( "output", cv2_img )
            cv2.waitKey(0)
            # plt.show()


if __name__ == '__main__':
    main()
    os._exit(0)
