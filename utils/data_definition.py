#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""!
@file DetaDefinition.py
@brief データ定義ファイル

@author: Koji Yamamoto / created on 2020/06/03
@copyright 2020 GlobalWalkers.inc. All rights reserved.
@modified: Min Thaw Zin / modified on 2021/09/17
"""
from enum import Enum

class MatrixKind(Enum):
    """!
    @brief confusion matrixの表示種別
    """
    All = 1      # 元画像
    One = 2      # マスク画像(アノテーションデータ)

class ClassKind(Enum):
    """!
    @brief MaskRCNNの検出対象クラス種別
    """
    Anchor = 0
    A1 = 1
    A2 = 2

class InputKind(Enum):
    """!
    @brief 入力種別
    """
    Movie = 1 # 動画
    Image = 2 # 画像

class FileStatus(Enum):
    """!
    @brief フォルダ種別
    """
    Origin = 0      # 元画像
    Mask = 1        # マスク画像(アノテーションデータ)
    T_Overlap = 2   # 元画像とマスク画像の重畳表示画像（一時保存）
    Overlap = 3     # 元画像とマスク画像の重畳表示画像
    P_Mask = 4      # 推論結果画像
    P_Overlap = 5   # 元画像と推論結果画像の重畳表示画像

class ImageType(Enum):
    """!
    @brief 画像種別
    """
    Origin = 0  # 元画像
    Overlap = 1 # オーバラップ画像

class ColorType():
    """!
    @brief 画像種別
    """
    def __init__(self):
        self.__color_list = [[128,128,128], # 0  anchor　灰色　アンカー
                             [0,255,0],     # 1  a1　緑色　アンカー剥離
                             [0,255,255]]                                            # 9  無し
    def get_color(self, index):
        """!
        @brief 色情報取得
        @param[in] type 色種別
        @return 指定クラスの色情報
        """
        return self.__color_list[index]

    def get_color_list(self):
        """!
        @brief 色情報リスト取得
        @return 色情報リスト
        """
        return self.__color_list
