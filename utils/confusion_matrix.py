"""
@file ConfusionMatrix.py
@brief confusion matrixを算出する

@author: Shunsuke Hishida / created on 2020/06/01
@copyright 2020 GlobalWalkers.inc. All rights reserved.
@modified: Min Thaw Zin / modified 2021/09/17
"""
import glob
import os
import warnings
import sklearn.exceptions
warnings.filterwarnings("ignore", category=sklearn.exceptions.UndefinedMetricWarning)
import cv2
import numpy as np
from sklearn.metrics import confusion_matrix, precision_score, recall_score, f1_score

import data_definition

class ConfusionMatrix(object):
    def __init__(self):
        """!
        @brief Construction
        """
        ct = data_definition.ColorType()
        self.__color_list = ct.get_color_list()
        self.__cls_num = len(self.__color_list) + 1
        self.__matrix_label = [i for i in range(self.__cls_num)]
        self.__gt_judge = []
        self.__pd_judge = []

    ####################################################
    # ↓メイン関数として用いない場合はこの関数を呼び出す
    ####################################################

    def generate_confusion_matrix(self, correct_msk_list, prediction_msk_list, iou_threshold):
        """!
        @brief コンフュージョンマトリックスの生成
        @param correct_msk_list         ground truthのパスリスト.
        @param prediction_msk_list      prediction maskのパスリスト.
        @return all_cm                  読み込んだリスト全体でのコンフュージョンマトリックス
        @return each_cm_list            各マスク画像に対するコンフュージョンマトリックスをリストに格納したもの
        """
        each_cm_list = []
        all_csv_list = []
        for index, correct_msk_path in enumerate(correct_msk_list):
            prediction_msk_path = prediction_msk_list[index]
            basename = os.path.basename(prediction_msk_path)
            correct_msk = cv2.imread(correct_msk_path)
            prediction_msk = cv2.imread(prediction_msk_path)
            correct_list = self.generate_cls_list(correct_msk)
            prediction_list = self.generate_cls_list(prediction_msk)
            gt_judge, pd_judge, csv_list = self.generate_judge_list(correct_list, prediction_list, correct_msk, prediction_msk, basename, iou_threshold)
            self.__gt_judge.extend(gt_judge)
            self.__pd_judge.extend(pd_judge)
            all_csv_list.extend(csv_list)
            each_cm = self.generate_cm(gt_judge, pd_judge)
            each_cm_list.append(each_cm)
        all_cm = self.generate_cm(self.__gt_judge, self.__pd_judge)
        print("Confustion Matrix Results")
        print("=========================")
        for id,cm in enumerate(all_cm):
            try:
                print(f"{str(data_definition.ClassKind(id)).split('.')[1]}: {cm}")
            # for background class
            except ValueError:
                print(f"Background: {cm}")
        return all_cm, each_cm_list, all_csv_list


    def generate_judge_list(self, correct_list, prediction_list, correct_msk, prediction_msk, basename, iou_threshold):
        """!
        @brief 各gt, pdでのiouを求めリスト化する。
        @param  correct_list (list)
        @param  prediction_list (list)
        @param  correct_msk (numpy.ndarray)(BGR)
        @param  prediction_msk (numpy.ndarray)(BGR)
        @param  basename (str)
        @param  iou_threshold
        @return gt_judge (list)
        @return pd_judge (list)
        """
        # ck = data_definition.ClassKind()
        iou_threshold = iou_threshold / 100
        gt_judge = []
        pd_judge = []
        csv_list = []
        app_gt_judge = gt_judge.append
        app_pd_judge = pd_judge.append
        app_csv_list = csv_list.append
        gt_status = [False] * len(correct_list)
        for c_index, correct in enumerate(correct_list):
            for prediction in prediction_list:
                iou = self.calc_msk_iou(correct, prediction, correct_msk, prediction_msk)
                if iou > iou_threshold:
                    correct[-1] = True
                    prediction[-1] = True
                    if correct[1] == prediction[1] and gt_status[c_index]:
                        app_csv_list([basename, str(data_definition.ClassKind(correct[1])).split('.')[1], str(data_definition.ClassKind(prediction[1])).split('.')[1], correct[0], prediction[0], iou])
                        continue
                    elif correct[1] == prediction[1] and not gt_status[c_index]:
                        gt_status[c_index] = True
                    app_gt_judge(correct[1])
                    app_pd_judge(prediction[1])
                app_csv_list([basename, str(data_definition.ClassKind(correct[1])).split('.')[1], str(data_definition.ClassKind(prediction[1])).split('.')[1], correct[0], prediction[0], iou])
            if not correct[-1]:
                app_gt_judge(correct[1])
                app_pd_judge(3)
        for prediction in prediction_list:
            if not prediction[-1]:
                app_gt_judge(3)
                app_pd_judge(prediction[1])
        return gt_judge, pd_judge, csv_list


    def generate_cm(self, gt_judge, pd_judge):
        """!
        @brief      confusion matrixの作成
        @param  gt_judge (list)
        @param  pd_judge (list)
        @return
        """
        cm = confusion_matrix(gt_judge, pd_judge, labels=self.__matrix_label)
        cm_list = cm.tolist()
        rs = recall_score(gt_judge, pd_judge, labels=self.__matrix_label, average=None)
        ps = precision_score(gt_judge, pd_judge, labels=self.__matrix_label, average=None)
        f1 = f1_score(gt_judge, pd_judge, labels=self.__matrix_label, average=None)
        for index in range(self.__cls_num):
            cm_list[index].append(round(rs[index], 2))
            cm_list[index].append(round(ps[index], 2))
            cm_list[index].append(round(f1[index], 2))
        return cm_list


    def generate_cls_list(self, msk, area_thresh=100):
        """!
        @brief              mask内に含まれる、クラスのbbox, クラス名の数分リストに格納して返す。
        @param msk          マスク画像
        @return cls_list    マスク画像に対して検出したクラス分を格納したリスト。
                            ex) [[[xmin1, ymin1, xmax1, ymax1], cls_name1, 組み合わせstatus(default=False)], [[xmin2, ymin2, xmax2, ymax2], cls_name2, 組み合わせstatus(default=False)]]
        @return color_list  各マスク画像に含まれているクラスのインデックスを画像ごとにリスト化して、更にリストに格納したもの
        """
        cls_list = []
        app_cls = cls_list.append
        for index, color in enumerate(self.__color_list):
            copy_msk = msk.copy()
            bin_msk = self.binaryzation(copy_msk, color)
            contours = cv2.findContours(bin_msk, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)[0]
            contours = list(filter(lambda x: cv2.contourArea(x) > area_thresh, contours))
            if len(contours) == 0:
                continue
            for single_contour in contours:
                each_bbox = self.generate_bbox(single_contour)
                each_cls_list = [each_bbox, index, False]
                app_cls(each_cls_list)
        return cls_list


    def generate_bbox(self, contour):
        """!
        @brief 対象のコンタからbounding boxを生成する。
        @param (numpy.ndarray)
        @bbox [x_min, y_min, x_max, y_max]
        """
        x_min, y_min, w, h = cv2.boundingRect(contour)
        x_max = x_min + w
        y_max = y_min + h
        bbox =  [x_min, y_min, x_max, y_max]
        return bbox


    def binaryzation(self, bgr_img, target_color):
        """!
        @brief              bgr形式の画像を2値化
        @param bgr_img      2値化する画像のデータ(numpy.ndarray)
        @color              白色にする対象の色(クラス)
        """
        white = (255, 255, 255)
        bgr_img[np.where((bgr_img == target_color).all(axis=2))] = white
        bin_img = cv2.inRange(bgr_img, white, white)
        return bin_img


    def count_pixels(self, bbox, color, mask):
        """!
        @brief         特定の色のpixel数を数える
        @param bbox (list)
        @param color (int)
        @param mask (numpy.ndarray)
        @return pixels (int)
        """
        x_min = bbox[0]
        y_min = bbox[1]
        x_max = bbox[2]
        y_max = bbox[3]
        img = mask[y_min:y_max, x_min:x_max, :]
        bin_msk = self.binaryzation(img, self.__color_list[color])
        pixels = cv2.countNonZero(bin_msk)
        return pixels


    def count_overlap_pixels(self, gt_color, gt_msk, pd_color, pd_msk, bbox):
        """!
        @brief      重なっている範囲のpixel数を求める
        @param  gt_color (int)
        @param  gt_msk (numpy.ndarray)
        @param  pd_color (int)
        @param  pd_msk (numpy.ndarray)
        """
        pixels = 0
        x_min = bbox[0]
        y_min = bbox[1]
        x_max = bbox[2]
        y_max = bbox[3]
        if x_min >= x_max or y_min >= y_max:
            return pixels
        gt = gt_msk[y_min:y_max, x_min:x_max, :]
        pd = pd_msk[y_min:y_max, x_min:x_max, :]
        bin_gt = self.binaryzation(gt, self.__color_list[gt_color])
        bin_pd = self.binaryzation(pd, self.__color_list[pd_color])
        bin_overlap = cv2.bitwise_and(bin_gt, bin_pd)
        pixels = cv2.countNonZero(bin_overlap)
        return pixels


    def calc_msk_iou(self, gt_msk_list, pd_msk_list, correct_msk, prediction_msk):
        """!
        @brief              maskのIoUを計算する
        @param gt_msk_list (list)   ground truthの検出されたクラス情報
        @param pd_msk_list (list)   predictionの検出されたクラス情報
        @param correct_msk (numpy.ndarray)  ground truth のmask
        @param prediction_msk (numpy.ndarray)  prediction のmask
        """
        gt_bbox = gt_msk_list[0]
        gt_color = gt_msk_list[1]
        pd_bbox = pd_msk_list[0]
        pd_color = pd_msk_list[1]
        overlap_bbox = [max(gt_bbox[0], pd_bbox[0]), max(gt_bbox[1], pd_bbox[1]), min(gt_bbox[2], pd_bbox[2]), min(gt_bbox[3], pd_bbox[3])]
        gt_pixel_num = self.count_pixels(gt_bbox, gt_color, correct_msk)
        pd_pixel_num = self.count_pixels(pd_bbox, pd_color, prediction_msk)
        overlap_pixel_num = self.count_overlap_pixels(gt_color, correct_msk, pd_color, prediction_msk, overlap_bbox)
        iou = np.round(overlap_pixel_num / (gt_pixel_num + pd_pixel_num - overlap_pixel_num), 2)
        return iou

    ####################################################################################
    # ↓当該ファイルがコマンドラインからスクリプトとして実行された場合に呼び出される関数
    ####################################################################################

    def get_file_path_list(self, dir_path, ext='png', recursive=True):
        """!
        @brief Get file path list
        """
        ret_list= []
        if not os.path.exists(dir_path):
            print('[ERROR][AugmentationSupporter][get_file_path_list] Not exists specified directory path.')
            return ret_list

        if recursive == True:
            path = os.path.join('{}/**/*.{}'.format(dir_path, ext))
        else:
            path = os.path.join('{}/*{}'.format(dir_path, ext))
        found_path_list = glob.glob(path, recursive=recursive)
        return found_path_list


    def main(self):
        """!
        @brief Main function
        """
        # print('[Main function]')
        load_path = './confusion_dataset/'
        correct_msk_path = os.path.join(load_path, 'correct_mask')
        prediction_msk_path = os.path.join(load_path, 'prediction_mask')
        correct_list = sorted(self.get_file_path_list(correct_msk_path, ext='png', recursive=True))
        prediction_list = sorted(self.get_file_path_list(prediction_msk_path, ext='png', recursive=True))
        # Hard to debug with try and except so removed it for now
        """
        try:
            if len(correct_list) == len(prediction_list):
                self.generate_confusion_matrix(correct_list, prediction_list, iou_threshold=30)
        except Exception as e:
            print(e)
            print('[ERROR] number of correct_list does not correspond to that of prediction_list.')
        """
        print(len(correct_list))
        print(len(prediction_list))
        if len(correct_list) == len(prediction_list):
            self.generate_confusion_matrix(correct_list, prediction_list, iou_threshold=30)
        else:
            print('[ERROR] number of correct_list does not correspond to that of prediction_list.')

if __name__ == '__main__':
    cm = ConfusionMatrix()
    cm.main()
